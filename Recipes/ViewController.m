//
//  ViewController.m
//  Recipes
//
//  Created by Jordan Hipwell on 1/21/15.
//  Copyright (c) 2015 Jordan Hipwell. All rights reserved.
//

#import "ViewController.h"
#import <QuartzCore/QuartzCore.h>

@interface ViewController ()

@end

@implementation ViewController

static NSString * RecipeTitleID = @"recipeTitle";
static NSString * RecipeImageID = @"recipeImage";
static NSString * RecipeInstructionsID = @"recipeInstructions";

- (void)viewDidLoad {
    [super viewDidLoad];

    NSArray *recipeData = [self recipeData];
    
    //setup UI
    UIScrollView *mainScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    mainScrollView.contentSize = CGSizeMake(self.view.frame.size.width * recipeData.count, self.view.frame.size.height);
    mainScrollView.pagingEnabled = YES;
    [self.view addSubview:mainScrollView];
    
    for (int i = 0; i < recipeData.count; i++) {
        //show recipe image full screen
        UIImageView *backgroundImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:recipeData[i][RecipeImageID]]];
        backgroundImageView.contentMode = UIViewContentModeScaleAspectFill;
        backgroundImageView.clipsToBounds = YES;
        backgroundImageView.frame = CGRectMake(i * mainScrollView.frame.size.width, 0, mainScrollView.frame.size.width, mainScrollView.frame.size.height);
        [mainScrollView addSubview:backgroundImageView];
        
        //add blur above image
        UIBlurEffect *blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleLight];
        UIVisualEffectView *effectView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
        effectView.frame = mainScrollView.frame;
        [backgroundImageView addSubview:effectView];
        
        //page scroll view
        UIScrollView *pageScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(i * mainScrollView.frame.size.width, 0, mainScrollView.frame.size.width, mainScrollView.frame.size.height)];
        pageScrollView.contentSize = CGSizeMake(pageScrollView.frame.size.width, self.view.frame.size.height * 5);
        [mainScrollView insertSubview:pageScrollView aboveSubview:backgroundImageView];
        
        //recipe title
        UILabel *recipeLabel = [[UILabel alloc] init];
        recipeLabel.text = recipeData[i][RecipeTitleID];
        recipeLabel.textAlignment = NSTextAlignmentCenter;
        recipeLabel.textColor = [UIColor whiteColor];
        recipeLabel.numberOfLines = 2;
        recipeLabel.font = [UIFont systemFontOfSize:25];
        recipeLabel.adjustsFontSizeToFitWidth = YES;
        recipeLabel.minimumScaleFactor = 0.5;
        recipeLabel.layer.shadowColor = [UIColor colorWithWhite:150/255.0 alpha:1.0].CGColor;
        recipeLabel.layer.shadowOffset = CGSizeMake(0, 1);
        recipeLabel.layer.shadowRadius = 2.0;
        recipeLabel.layer.shadowOpacity = 0.5;
        recipeLabel.layer.masksToBounds = NO;
        
        //recipe image
        UIImageView *imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:recipeData[i][RecipeImageID]]];
        imageView.contentMode = UIViewContentModeScaleAspectFill;
        imageView.clipsToBounds = YES;
        
        //recipe instructions
        UITextView *instructionsTextView = [[UITextView alloc] init];
        instructionsTextView.editable = NO;
        instructionsTextView.backgroundColor = [UIColor clearColor];
        instructionsTextView.font = [UIFont systemFontOfSize:14];
        instructionsTextView.text = recipeData[i][RecipeInstructionsID];
        instructionsTextView.scrollEnabled = NO;
        instructionsTextView.frame = CGRectMake(20, 0, pageScrollView.frame.size.width - 40, CGFLOAT_MAX);
        [instructionsTextView sizeToFit];
        
        //place title and image in different positions depending on current page index
        if (i != 2) {
            recipeLabel.frame = CGRectMake(20, 50, pageScrollView.frame.size.width - 40, 80);
            [pageScrollView addSubview:recipeLabel];
            
            imageView.frame = CGRectMake(0, recipeLabel.frame.origin.y + recipeLabel.frame.size.height + 25, pageScrollView.frame.size.width, pageScrollView.frame.size.height / 3.5);
            [pageScrollView addSubview:imageView];
            
            instructionsTextView.frame = CGRectMake(20, imageView.frame.origin.y + imageView.frame.size.height + 25, pageScrollView.frame.size.width - 40, instructionsTextView.frame.size.height);
            [pageScrollView addSubview:instructionsTextView];
        } else { //page 3
            imageView.frame = CGRectMake(0, 50, pageScrollView.frame.size.width, pageScrollView.frame.size.height / 3.5);
            [pageScrollView addSubview:imageView];
            
            recipeLabel.frame = CGRectMake(20, imageView.frame.origin.y + imageView.frame.size.height + 25, pageScrollView.frame.size.width - 40, 80);
            [pageScrollView addSubview:recipeLabel];
            
            instructionsTextView.frame = CGRectMake(20, recipeLabel.frame.origin.y + recipeLabel.frame.size.height + 25, pageScrollView.frame.size.width - 40, instructionsTextView.frame.size.height);
            [pageScrollView addSubview:instructionsTextView];
        }
        
        //allow scrolling entire page
        pageScrollView.contentSize = CGSizeMake(pageScrollView.frame.size.width, instructionsTextView.frame.origin.y + instructionsTextView.frame.size.height + 25);
    }
}

- (NSArray *)recipeData {
    return @[ @{ RecipeTitleID: @"Raspberry Swirled Lemon Cheesecake Bars", RecipeImageID: @"rasp", RecipeInstructionsID: @"Yield: 12 bars\n\nIngredients\n\nFor the Crust:\n1 1/2 cups graham cracker crumbs\n4T butter\n\nFor the Cheesecake:\n16 ounces cream cheese- room temperature\n1/2 cup sugar\n2 tablespoons lemon juice\nZest of a lemon\n4 tablespoons milk\n2 eggs- room temperature\n1 teaspoon vanilla extract\n\nFor the Raspberry Swirl:\n4 oz. of fresh raspberries (about 1/2 cup)\n2 tablespoons sugar\n\nInstructions\n  1. Preheat oven to 350F. Line an 8x8 baking pan with parchment paper, criss-crossing two rectangular pieces so there is an overhang on either side for easy lifting. Set aside.\nFor the crust\n  2. In a medium sized bowl mix together the crumbs and butter with a spoon until moist. Press into the prepared pan and bake for about 10 minutes or until crust is set around the edges.\n  3. Meanwhile make the filling:\n  4. Beat together the white sugar, and cream cheese until smooth.\n  5. Add the egg, milk, lemon juice, and vanilla and beat until smooth and creamy. Spread filling mixture over baked crust.\nFor the Raspberry Purée:\n  6. In a food processor or blender, purée the raspberries until smooth. Press through a fine mesh sieve, getting as much liquid out as possible. Discard the solids. Stir in the sugar.\n  7. Dot the raspberry mixture over the filling and use a toothpick or skewer to swirl together.\n  8. Bake for 30 minutes or until set around the edges and slightly jiggly in the center. Cool completely on a wire rack and then chill until cold before serving." },
              @{ RecipeTitleID: @"Chocolate Fruit Cups", RecipeImageID: @"choc", RecipeInstructionsID: @"Ingredients\n1 cup chocolate chips (preferably dark chocolate one)\n10 paper cupcake liners\n2 cups fresh strawberries or any of your favorite fruit; sliced in half\nOil in a spray container (or cooking spray)\n\nInstructions\n  1. Melt chocolate in a microwave safe container (timing varies by microwave, start with 45 second intervals and depending on your microwave, see if you need more.\n  2. Lightly spray cupcake liners with oil before spooning melted chocolate inside. Carefully brush the chocolate mixture inside the liner with a spoon, creating a thin layer about ¼ inch or less. Make sure the chocolate fully covers the side of the cupcake liners.\n  3. Place on a plate in the refrigerator or freezer for 30 minutes and allow the chocolate to harden. Once hardened, check the cupcake liners to make sure there are no thin spots. If so, brush on more chocolate.\n  4. When the chocolate has hardened, peel the paper liners. You should have perfect chocolate cups. Fill each with sliced strawberries or your favorite fruit, drizzle with melted chocolate and enjoy!!" },
              @{ RecipeTitleID: @"Salted Caramel and Chocolate Pecan Pie Bars", RecipeImageID: @"pecan", RecipeInstructionsID: @"INGREDIENTS:\n\nCrust\n1/2 cup unsalted butter, very soft (1 stick)\n1 cup all-purpose flour\n1/4 cup confectioners' sugar\n1 tablespoon cornstarch\npinch salt, optional and to taste\nFilling\n8 ounces roasted salted pecans, halves or pieces okay; about 2 cups (I used Trader Joe's roasted salted halves)\n1 cup semi-sweet chocolate chips\n1/2 cup unsalted butter (1 stick)\n1 cup light brown sugar, packed\n1/3 cup whipping cream or heavy cream\n1 tablespoon vanilla extract\n1/2 teaspoon salt, or to taste\n\nDIRECTIONS:\n\n  1. Preheat oven to 350F. Line an 8-by-8-inch baking pan with aluminum foil, spray with cooking spray; set aside. I strongly urge lining the pan with foil for easier cleanup.\n  2. Crust - In a large bowl, combine all crust ingredients and using two forks or your hands (I find hands easier), cut butter into dry ingredients until evenly distributed and pea-sized lumps and sandy bits form. The softer the butter is, the quicker and easier it is.\n  3. Turn mixture out into prepared pan and pack down firmly with a spatula or hands to create an even, uniform, flat crust layer.\n  4. Filling - Evenly sprinkle the pecans.\n  5. Evenly sprinkle with the chocolate chips; set pan aside.\n  6. In a large, microwave-safe bowl, combine 1/2 cup butter, brown sugar, whipping cream, and heat on high power for 1 minute to melt.\n  7. Remove bowl from micro, and whisk until mixture is smooth; it's okay if butter hasn't completely melted.\n  8. Return bowl to microwave and heat for 1 minute on high power.\n  9. Remove bowl from micro, and whisk until mixture is smooth.\n  10. Whisk in the vanilla and salt.\n  11. Slowly and evenly pour the caramel sauce over the chocolate chips and pecans.\n  12. Place pan on a cookie sheet (as insurance against overflow) and bake for about 30 to 32 minutes, or until caramel is bubbling vigorously around edges, with bubbling to a lesser degree in center. Allow bars to cool in pan on a wire rack for at least 3 hours, or overnight (cover with a piece of foil and/or put pan inside large Ziplock), before slicing and serving. Bars will keep airtight at room temperature for up to 1 week, or in the freezer for up to 6 months." },
              @{ RecipeTitleID: @"Lazy Ice Cream Cake", RecipeImageID: @"ice-cream", RecipeInstructionsID: @"Ingredients\n12 ice cream sandwiches, any flavor\nIce cream, any flavor- softened\nIce cream topping, such as caramel, hot fudge, strawberry, etc.\nWhipped Topping\n\nInstructions\n  1. Line an 8\" x 8\" pan with saran wrap. Using half the sandwiches, make a single layer.\n  2. Cover the ice cream sandwiches with softened ice cream- try to make an even layer. (used about ⅓ of a tub of Cookies 'n Cream.)\n  3. Cover the ice cream with a thin layer of hot fudge\n  4. Finish the layers off with the remaining ice cream sandwiches.\n  5. Throw the pan in the fridge for about an hour so the ice cream can harden.\n  6. Once the ice cream has hardened, invert the pan onto the plate you'll serve it from.\n  7. \"Frost\" the cake with the whipped topping.\n  8. Try to get fancy and make lines of caramel & chocolate sauce, dragging a toothpick in alternating directions to create the design.\n  9. Return the cake to the freezer until ready to serve." },
              @{ RecipeTitleID: @"Loaded Oatmeal Chocolate Chip Cookies", RecipeImageID: @"oat", RecipeInstructionsID: @"INGREDIENTS:\n\n1 large egg\n1/2 cup unsalted butter (1 stick)\n1/2 cup light brown sugar, packed\n1/4 cup granulated sugar\n1 tablespoon vanilla extract\n1 cup old-fashioned whole rolled oats (not instant or quick cook)\n1 cup all-purpose flour\n1/2 cup sweetened shredded coconut\n1/2 cup chopped walnuts (pecans may be substituted; or nuts may be omitted)\n1/2 teaspoon baking soda\n1/4 teaspoon salt, or to taste\n1 heaping cup semi-sweet chocolate chips\n\nDIRECTIONS:\n\n  1. To the bowl of a stand mixer fitted with the paddle attachment (or large mixing bowl and electric mixer) combine the egg, butter, sugars, vanilla, and beat on medium-high speed until creamed and well combined, about 4 minutes.\n  2. Stop, scrape down the sides of the bowl, and add the oats, flour, coconut, walnuts, baking soda, salt to taste, and beat on low speed until just combined, about 1 minute.\n  3. Stop, scrape down the sides of the bowl, and add the chocolate chips, and beat on low speed until just combined, about 30 seconds.\n  4. Using a large cookie scoop, 1/4-cup measure, or your hands, form approximately 12 equal-sized mounds of dough, roll into balls, and flatten slightly. Tip - Strategically place a few chocolate chips on top of each mound of dough by taking chips from the underside and adding them on top.\n  5. Place mounds on a large plate or tray, cover with plasticwrap, and refrigerate for at least 2 hours, up to 5 days. Do not bake with unchilled dough because cookies will bake thinner, flatter, and be more prone to spreading.\n  6. Preheat oven to 350F, line a baking sheet with a Silpat or spray with cooking spray. Place dough mounds on baking sheet, spaced at least 2 inches apart (I bake 8 cookies per sheet) and bake for about 11 t0 13 minutes (for super soft cookies, longer for more well-done cookies), or until edges have set and tops are just set, even if slightly undercooked, pale, and glossy in the center; don't overbake. Cookies firm up as they cool. Allow cookies to cool on baking sheet for about 10 minutes before serving. I let them cool on the baking sheet and don't use a rack.\n  7. Cookies will keep airtight at room temperature for up to 1 week or in the freezer for up to 6 months. Alternatively, unbaked cookie dough can be stored in an airtight container in the refrigerator for up to 5 days, or in the freezer for up to 4 months, so consider baking only as many cookies as desired and save the remaining dough to be baked in the future when desired." } ];
}

@end
